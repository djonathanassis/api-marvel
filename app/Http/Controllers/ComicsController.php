<?php

namespace App\Http\Controllers;

use App\Services\Marvel\Marvel;
use App\ViewModels\CharacterViewModel;
use App\ViewModels\ComicsViewModel;
use App\ViewModels\ComicViewModel;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Inertia\Inertia;
use Inertia\Response;

class ComicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $limit = 10;
        $page = $request->input("page") ?? 1;

        $offset = ($page - 1) * $limit;

        $comics = (new Marvel())->comics()->list([
            'format' => 'comic',
            'formatType' => 'comic',
            'dateDescriptor' => 'thisMonth',
            'orderBy' => 'title',
            'limit' => $limit,
            'offset' => $offset
        ]);

        $comicsView = new ComicsViewModel($comics->data->results);

        $array = $comicsView->comicsLastMonth()->all();


        $paginator = new Paginator($array, $comics->data->total, $limit, $page, [
            'path' => $request->url(),
            'query' => $request->query(),
        ]);

        return Inertia::render('Comics/Comics-list', ['comics' => $paginator]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id): Response
    {
        $comic = (new Marvel())->comics()->show($id);
        $character = (new Marvel())->comics()->characters($id);

        $collectionComic = collect($comic->data->results);
        $collectionCharacter = collect($character->data->results);

        $comicView = new ComicViewModel($collectionComic->sole(), $collectionCharacter->all());

        return Inertia::render('Comics/Comics-show', ['comic' => $comicView->comic()->all(),
            'characters' => $comicView->characters()->all()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
