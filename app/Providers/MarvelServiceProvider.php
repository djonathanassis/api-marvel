<?php

namespace App\Providers;

use App\Services\Marvel\Client;
use Illuminate\Support\ServiceProvider;

class MarvelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            Client::class,
             function (){
                return new Client(
                     config( 'services.api-marvel.url'),
                     config( 'services.api-marvel.public_key'),
                     config( 'services.api-marvel.private_key'),
                );
            }
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
