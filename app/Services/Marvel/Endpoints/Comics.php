<?php

namespace App\Services\Marvel\Endpoints;

use App\Services\Marvel\HttpClient;

class Comics extends HttpClient
{

    public function list(array $options = []): object
    {
        return $this->http->get('/comics', $options);
    }

    public function show(int $id, array $options = []): object
    {
        return $this->http->get("/comics/${id}", $options);
    }

    public function characters(int $id, array $options = []): object
    {
        return $this->http->get("/comics/${id}/characters", $options);
    }


}
