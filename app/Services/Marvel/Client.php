<?php

declare(strict_types=1);

namespace App\Services\Marvel;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class Client extends Http
{
    protected string $baseUrl;
    protected string $publicApiKey;
    protected string $privateApiKey;

    public function __construct(
        $baseUrl,
        $publicApiKey,
        $privateApiKey)
    {
        $this->baseUrl = $baseUrl;
        $this->publicApiKey = $publicApiKey;
        $this->privateApiKey = $privateApiKey;

    }

    public function __call($method, $args)
    {
        if (count($args) < 1) {
            throw new \InvalidArgumentException(
                'Magic request methods require a URI and optional options array'
            );
        }

        $url = $args[0];

        $options = $args[1] ?? [];

        return $this->request($method, $url, $options);
    }


    /**
     * @throws \Exception
     */
    public function request($method, $url, $options)
    {
        $request = $this->buildRequest($options);

        $response = $request->send($method, $url);

        return \Response::json($response->json())->getData();

    }

    private function getHash(): string
    {
        return md5(time() . $this->privateApiKey . $this->publicApiKey);
    }

    private function buildRequest($options): PendingRequest
    {
        $array = array_merge(
            [
                'ts' => time(),
                'apikey' => $this->publicApiKey,
                'hash' => $this->getHash(),
            ],
            $options);

        return Http::baseUrl($this->baseUrl)
            ->withHeaders(
                [
                    'Accept' => 'application/json',
                    'read_timeout' => 5,
                    'timeout' => 5,
                ]
            )
            ->withOptions(
                [
                    'query' => $array
                ]
            );
    }
}
