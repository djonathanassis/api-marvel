<?php

namespace App\Services\Marvel;

use App\Services\Marvel\Endpoints\Comics;

class Marvel
{
    protected $client;

    public function __construct()
    {
        $this->client = resolve(Client::class);
    }

    public function comics(): Comics
    {
        return new Comics($this->client);
    }

}
