<?php

namespace App\Services\Marvel;

abstract class HttpClient
{
    protected Client $http;

    public function __construct(Client $http)
    {
        $this->http = $http;
    }
}
