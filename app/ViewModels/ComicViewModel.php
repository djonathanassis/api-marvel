<?php

namespace App\ViewModels;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Spatie\ViewModels\ViewModel;

class ComicViewModel extends ViewModel
{
    protected $comic;
    protected $characters;

    public function __construct($comic, $characters)
    {
        $this->comic = $comic;
        $this->characters = $characters;
    }

    public function comic(): Collection
    {
        return collect($this->comic)->merge([
            'thumbnail' => $this->comic->thumbnail->path.'/portrait_uncanny.'.$this->comic->thumbnail->extension,
            'dates' => Carbon::parse($this->comic->dates[0]->date)->format('M d, Y'),
            'prices' => $this->prices($this->comic->prices),
            'creators' => collect($this->comic->creators->items),
            'urls' => $this->urls($this->comic->urls),
        ])->only([
            'id', 'title', 'dates', 'pageCount', 'prices', 'thumbnail', 'description', 'creators', 'urls',
        ]);
    }

    public function characters(): Collection
    {
        return collect($this->characters)->map(function($character){
            return collect($character)->merge([
                'thumbnail' => $character->thumbnail->path.'/portrait_uncanny.'.$character->thumbnail->extension
            ])->only([
                'id', 'name', 'thumbnail',
            ]);
        });
    }

    public function prices($prices): Collection
    {
        return collect($prices)->mapWithKeys(function($price){
            return [$price->type => $price->price];
        });
    }

    public function urls($urls): Collection
    {
        return collect($urls)->mapWithKeys(function($url){
            return [$url->type => $url->url];
        });
    }
}
