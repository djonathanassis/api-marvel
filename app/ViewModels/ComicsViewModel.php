<?php

namespace App\ViewModels;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Spatie\ViewModels\ViewModel;

class ComicsViewModel extends ViewModel
{
    protected $comicsLastMonth;

    public function __construct($comicsLastMonth)
    {
        $this->comicsLastMonth = $comicsLastMonth;
    }

    public function comicsLastMonth(): Collection
    {
        return collect($this->comicsLastMonth)->map(function($comic){
            return collect($comic)->merge([
                'thumbnail' => $comic->thumbnail->path.'/portrait_uncanny.'.$comic->thumbnail->extension,
                'dates' => Carbon::parse($comic->dates[0]->date)->format('M d, Y'),
                'prices' => $this->prices($comic->prices),
            ])->only([
                'id', 'title', 'prices', 'dates', 'thumbnail',
            ]);
        });
    }

    public function prices($prices): Collection
    {
        return collect($prices)->mapWithKeys(function($price){
            return [$price->type => $price->price];
        });
    }
}
