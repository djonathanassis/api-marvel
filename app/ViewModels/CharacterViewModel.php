<?php

namespace App\ViewModels;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Spatie\ViewModels\ViewModel;

class CharacterViewModel extends ViewModel
{
    protected $character;

    public function __construct($character)
    {
        $this->character =  $character;
    }

    public function character(): Collection
    {

        dd($this->character);

    }
}
