<?php

use App\Http\Controllers\ComicsController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ComicsController::class, 'index'])->name('comics');
Route::get('/comics/{id}', [ComicsController::class, 'show'])->name('comics.show');
Route::get('/cart', function (){
    return Inertia::render('Cart/Cart-show', []);
})->name('cart.show');
Route::get('/confirmation', function (){
    return Inertia::render('Cart/Confirmation', []);
})->name('confirmation');
