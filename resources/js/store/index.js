import { createStore } from 'vuex'

const getters = {
    listProducts: (state) => (comic) => {
       return state.products.filter(item => item.comic === comic)
    },

    listCart: state => {
        return state.cart.map(({id, quantity}) => {
            const dataItem = state.products.find(item => item.id === id)
            return {
                id,
                format: dataItem.format,
                thumbnail: dataItem.thumbnail,
                title: dataItem.title,
                quantity,
                price: dataItem.price,
                subTotal: (quantity * dataItem.price).toFixed(2)
            }
        })
    },

    numCartItems: (state, getters) => {
        return getters.listCart.reduce((num, item) => num + item.quantity, 0)
    },

    totalCart: (state, getters) => {
        return getters.listCart.reduce((total, item) => total + (item.quantity * item.price), 0)
    }

};

const actions = {

    async FETCH_ITEMS ({ commit, getters, state }, comic) {
       if(getters.listProducts(comic).length === 0){

           commit('LOAD_ITEMS', {data: comic.data, comic: comic});
       }
    },

    ADD_CART({ commit, state }, id){
        const productsItem = state.products.find(item => item.id === id)
        if (productsItem.stock > 0) {
            const cartItem = state.cart.find(item => item.id === id)
            if (!cartItem) {
                commit('ADD_ITEM_TO_CART', id)
            }else{
                commit('INCREMENT_ITEM_UNITS', id)
            }
            commit('DECREMENT_ITEM_STOCK', id)
        }
    },

    REMOVE_FROM_CART ({ commit, state }, id) {
        const cartItem = state.cart.find(item => item.id === id)
        commit('REMOVE_ITEM_FROM_CART', id)
        commit('INCREMENT_ITEM_STOCK', {id: id, quantity: cartItem.quantity})
    },

    VALIDATE_CHECKOUT ({ commit }){
        commit('CLEAR_ITEMS')
    },

    EMPTY_CART ({ commit }){
        // state.cart.forEach((item) => {
        //     commit('INCREMENT_ITEM_STOCK', {id: item.id, quantity: item.quantity})
        // })
        commit('CLEAR_ITEMS')
    }
};

const mutations = {

    LOAD_ITEMS (state, { data}) {
        for (let i = 0; i < data.length; i++) {
            const item = data[i];

            state.products.push({
                id: item.id,
                title:item.title,
                price: item.prices.printPrice > 0 ? item.prices.printPrice : 1.99,
                stock: Math.floor(Math.random() * 20),
                thumbnail: item.thumbnail
            })
        }
    },

    REMOVE_ITEM_FROM_CART (state, id) {
        const index = state.cart.findIndex(item => item.id === id)
        state.cart.splice(index, 1)
    },

    ADD_ITEM_TO_CART (state, id) {
        state.cart.push({
            id,
            quantity: 1
        })
    },

    CLEAR_ITEMS (state){
        state.cart = [];
    },

    INCREMENT_ITEM_UNITS (state, id) {
        const cartItem = state.cart.find(item => item.id === id)
        cartItem.quantity++
    },

    INCREMENT_ITEM_STOCK (state, {id, quantity}) {
        const dataItem = state.products.find(item => item.id === id)
        dataItem.stock = dataItem.stock + quantity
    },

    DECREMENT_ITEM_STOCK (state, id) {
        const dataItem = state.products.find(item => item.id === id)
        dataItem.stock--
    },

};

export default createStore({
    state: {
        products: [],
        cart: [],
        error: false,
        loading: true,
    },
    getters,
    actions,
    mutations
});
