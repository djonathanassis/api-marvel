
require('./bootstrap');

import {createApp, h} from 'vue'
import {createInertiaApp, Head, Link } from '@inertiajs/inertia-vue3'
import {InertiaProgress} from '@inertiajs/progress'
import store from './store'

InertiaProgress.init()

createInertiaApp({
    id: 'app',
    resolve: name => require(`./Pages/${name}`),
    setup({el, App, props, plugin}) {
        createApp({render: () => h(App, props)})
            .use(plugin)
            .use(store)
            .component('InertiaHead', Head)
            .component('InertiaLink', Link)
            .mixin({methods: {route: window.route}})
            .mount(el)
    },
})
