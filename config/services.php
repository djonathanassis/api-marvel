<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'api-marvel' => [
        'url' => env('MARVEL_BASE_URL', 'https://gateway.marvel.com/v1/public'),
        'public_key' => env('MARVEL_PUBLIC_KEY', '267c07cf54465ca1d49f4e707077fe2e'),
        'private_key' => env('MARVEL_PRIVATE_KEY', '6fb20ebfc8c29195df3861af6fdabc6ee0b9e922')
    ]
];
