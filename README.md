
## Laravel Marvel Api

Um projeto simples exibindo dados detalhes de quadrinhos, carrinho de compras.
Uma funcionalidade de checkout dos quadrinhos adicionados
Esta página foi construída para praticar o Laravel Framework e o Inertia.
Principalmente para entender como trabalhar com uma api externa com json, neste caso com a API Marvel.
## Tecnologia Utilizados 

```
📄 Laravel
📝 Tailwind CSS
📑 Vue.js 3
📑 Inertia.js

```
